package de.binaris.cleancode.solid.oc.bad;

import java.math.BigDecimal;

import de.binaris.cleancode.solid.oc.bad.claim.LifeInsuranceClaim;
import de.binaris.cleancode.solid.oc.bad.claim.VehicleInsuranceClaim;

public class InsuranceClaimManager {
	
	public void processVehicleInsuranceClaim(VehicleInsuranceClaim claim) {
		if(claim.isValid()) {
			System.out.println("VehicleInsuranceClaim is valid, initiating payment");		
		}else {
			System.out.println("VehicleInsuranceClaim is invalid, sueing policy holder");
		}
	}
	
	public void processLifeInsuranceClaim(LifeInsuranceClaim claim) {
		if(claim.isValid()) {
			System.out.println("LifeInsuranceClaim is valid, initiating payment");		
		}else {
			System.out.println("LifeInsuranceClaim is invalid, sueing policy holder");
		}
	}	
}
