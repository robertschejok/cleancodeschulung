package de.binaris.cleancode.solid.oc.good.claim;

public interface InsuranceClaim {
	boolean isValid();
	void setValid(boolean valid);
	String getName();
}
