package de.binaris.cleancode.solid.oc.good.claim;

public class VehicleInsuranceClaim implements InsuranceClaim{

	private boolean valid;
	public boolean isValid() {
		System.out.println("Validating VehicleInsuranceClaim");
		return this.valid;
	}
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	@Override
	public String getName() {
		return "Life Insurance Claim";
	}

}
