package de.binaris.cleancode.solid.oc.good.claim;

public class LifeInsuranceClaim implements InsuranceClaim{
	
	private boolean valid;
	public boolean isValid() {
		System.out.println("Validating LifeInsuranceClaim");
		return this.valid;
	}

	public void setValid(boolean b) {
		this.valid = valid;
	}

	@Override
	public String getName() {
		return "Life Insurance Claim";
	}
	
	
}
