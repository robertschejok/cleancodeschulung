package de.binaris.cleancode.solid.oc.good;

import de.binaris.cleancode.solid.oc.good.claim.InsuranceClaim;

public class InsuranceClaimManager {
	
	public void processInsuranceClaim(InsuranceClaim claim) {
		if(claim.isValid()) {
			System.out.println(claim.getName() + " is valid, initiating payment");		
		}else {
			System.out.println(claim.getName() + " is invalid, sueing policiy holder");
		}
	}
}
