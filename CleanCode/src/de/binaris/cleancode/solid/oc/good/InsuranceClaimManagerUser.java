package de.binaris.cleancode.solid.oc.good;

import de.binaris.cleancode.solid.oc.good.claim.LifeInsuranceClaim;
import de.binaris.cleancode.solid.oc.good.claim.VehicleInsuranceClaim;

public class InsuranceClaimManagerUser {

	public static void main(String[] args) {
		InsuranceClaimManager insuranceClaimManager = new InsuranceClaimManager();
		
		VehicleInsuranceClaim vehicleInsuranceClaim = new VehicleInsuranceClaim();
		vehicleInsuranceClaim.setValid(true);
		insuranceClaimManager.processInsuranceClaim(vehicleInsuranceClaim);
		
		LifeInsuranceClaim lifeInsuranceClaim = new LifeInsuranceClaim();
		lifeInsuranceClaim.setValid(false);
		insuranceClaimManager.processInsuranceClaim(lifeInsuranceClaim);
	}
}
