package de.binaris.cleancode.solid.srp.good.device;

public class CDPlayer extends Device {

	@Override
	public void play(String title) {
		if(title != null)
			System.out.println("Playing \"" + title + "\" with CD-Player");
	}

}
