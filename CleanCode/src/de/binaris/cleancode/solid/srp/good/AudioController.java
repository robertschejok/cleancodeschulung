package de.binaris.cleancode.solid.srp.good;

import de.binaris.cleancode.solid.srp.good.device.CDPlayer;
import de.binaris.cleancode.solid.srp.good.device.Device;

public class AudioController {

	private String title;
	private Device device;
	
	public static void main(String[] args) {
		AudioController ac = new AudioController();
		ac.setTitle("Rock around the Clock");
		ac.setDevice(new CDPlayer());
		ac.play();
	}
	
	private void setTitle(String title) {
		this.title = title;
	}
	
	private void setDevice(Device device) {
		this.device = device;
	}
	
	private void play() {
		this.device.play(title);
	}
	
	
	
}
