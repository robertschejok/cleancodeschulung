package de.binaris.cleancode.solid.srp.good.device;

public abstract class Device {
	public void play(String title) {}
}
