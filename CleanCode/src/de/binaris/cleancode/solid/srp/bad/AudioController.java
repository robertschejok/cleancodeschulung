package de.binaris.cleancode.solid.srp.bad;

public class AudioController {

	private String title;
	private Device device;
	
	public static void main(String[] args) {
		AudioController ac = new AudioController();
		ac.setTitle("Rock around the Clock");
		ac.setDevice(Device.CDPLAYER);
		ac.play();
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	private void setDevice(Device device) {
		this.device = device;
	}
	
	public void play() {
		if(this.title != null && this.device != null) {
			switch(this.device) {
				case CDPLAYER:
					playWithCDPlayer();
					break;
				case MINIDISC:
					playWithMiniDisc();
					break;
				default:
					System.err.println("Generische Fehlermeldung...");
					
			}
		}else {
			System.err.println("Es werden Titel und Ger�t ben�tigt");
		}
	}
	
	public void playWithCDPlayer() {
		System.out.println("Playing \"" + this.title + "\" with CD-Player");
	}
	
	public void playWithMiniDisc() {
		System.out.println("Playing \"" + this.title + "\" with MiniDisc-Player");
	}

	
}
