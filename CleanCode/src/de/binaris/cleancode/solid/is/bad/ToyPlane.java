package de.binaris.cleancode.solid.is.bad;

public class ToyPlane implements Toy {
	
	private double price;
	private String color;

	@Override
	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public void setColor(String color) {
		this.color = color;
	}

	@Override
	public void drive() {
		throw new RuntimeException("Flugzeuge fahren nicht");
	}

	@Override
	public void fly() {
		System.out.println("Fugzeug fliegt...");
	}
	
	@Override
	public String toString() {
		return "Flugzeug, " + this.color + ", f�r " + this.price + " Euro"; 
	}

	@Override
	public boolean canDrive() {
		return false;
	}

	@Override
	public boolean canFly() {
		return true;
	}

}
