package de.binaris.cleancode.solid.is.bad;

public class ToyHouse implements Toy {
	
	private double price;
	private String color;

	@Override
	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public void setColor(String color) {
		this.color = color;
	}

	@Override
	public void drive() {
		throw new RuntimeException("H�user fahren nicht");
	}

	@Override
	public void fly() {
		throw new RuntimeException("H�user fliegen nicht");
	}
	
	@Override
	public String toString() {
		return "Haus, " + this.color + " f�r " + this.price + " Euro"; 
	}

	@Override
	public boolean canDrive() {
		return false;
	}

	@Override
	public boolean canFly() {
		return false;
	}

}
