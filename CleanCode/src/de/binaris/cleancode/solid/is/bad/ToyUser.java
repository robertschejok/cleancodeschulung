package de.binaris.cleancode.solid.is.bad;

public class ToyUser {
	
	enum ToyType{
		CAR,
		PLANE,
		HOUSE,
		FLYING_CAR
	}

	public static void main(String[] args) {
		ToyUser toyUser = new ToyUser();
		
		ToyFlyingCar toyflyingCar = (ToyFlyingCar) toyUser.createToy(ToyType.FLYING_CAR, "blau", 25.00);
		toyUser.playWithToy(toyflyingCar);

		ToyPlane toyPlane = (ToyPlane) toyUser.createToy(ToyType.PLANE, "gelb", 30.00);
		toyUser.playWithToy(toyPlane);
		
		ToyCar toyCar = (ToyCar) toyUser.createToy(ToyType.CAR, "gr�n", 15.00);
		toyUser.playWithToy(toyCar);
		
		ToyHouse toyHouse = (ToyHouse) toyUser.createToy(ToyType.HOUSE, "rot", 100.00);
		toyUser.playWithToy(toyHouse);
	}
	
	public void playWithToy(Toy toy) {
		System.out.println(toy.toString());
		if(toy.canDrive()) {
			toy.drive();
		}
		if(toy.canFly()) {
			toy.fly();
		}
		System.out.print("\n");
	}
	
	public Toy createToy(ToyType type, String color, double price) {
		Toy toy = null;
		switch (type) {
			case CAR:
				toy = new ToyCar();
				break;
			case PLANE:
				toy = new ToyPlane();
				break;
			case HOUSE:
				toy = new ToyHouse();
				break;
			case FLYING_CAR:
				toy = new ToyFlyingCar();
				break;
			default:
				throw new RuntimeException("Unbekannter Spielzeugtyp.");
		}
		toy.setColor(color);
		toy.setPrice(price);
		return toy;
	}

}
