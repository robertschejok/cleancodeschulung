package de.binaris.cleancode.solid.is.bad;

public interface Toy {
	void setPrice(double price);
	void setColor(String color);
	boolean canDrive();
	void drive();
	boolean canFly();
	void fly();
	String toString();
}
