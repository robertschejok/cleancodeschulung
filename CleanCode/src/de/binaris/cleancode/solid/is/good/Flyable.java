package de.binaris.cleancode.solid.is.good;

public interface Flyable {
	void fly();
}
