package de.binaris.cleancode.solid.is.good;

public class ToyCar implements Toy, Drivable {

	private double price;
	private String color;

	@Override
	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public void setColor(String color) {
		this.color = color;
	}

	@Override
	public void drive() {
		System.out.println("Auto f�hrt...");
	}

	@Override
	public String toString() {
		return "Auto, " + this.color + " f�r " + this.price + " Euro";
	}
}
