package de.binaris.cleancode.solid.is.good;

public interface Toy {
	void setPrice(double price);
	void setColor(String color);
	String toString();
}
