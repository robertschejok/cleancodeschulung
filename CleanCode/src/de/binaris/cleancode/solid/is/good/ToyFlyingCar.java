package de.binaris.cleancode.solid.is.good;

public class ToyFlyingCar implements Toy, Drivable, Flyable {

	private double price;
	private String color;

	@Override
	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public void setColor(String color) {
		this.color = color;
	}

	@Override
	public void drive() {
		System.out.println("Fliegendes Auto f�hrt...");
	}

	@Override
	public void fly() {
		System.out.println("Fliegendes Auto fliegt...");
	}

	@Override
	public String toString() {
		return "Fliegendes Auto, " + this.color + ", f�r " + this.price + " Euro";
	}
}
