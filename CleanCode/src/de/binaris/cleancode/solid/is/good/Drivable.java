package de.binaris.cleancode.solid.is.good;

public interface Drivable {
	void drive();
}
