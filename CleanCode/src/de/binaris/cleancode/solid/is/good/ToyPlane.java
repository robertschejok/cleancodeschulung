package de.binaris.cleancode.solid.is.good;

public class ToyPlane implements Toy, Flyable {

	private double price;
	private String color;

	@Override
	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public void setColor(String color) {
		this.color = color;
	}

	@Override
	public void fly() {
		System.out.println("Fugzeug fliegt...");
	}

	@Override
	public String toString() {
		return "Flugzeug, " + this.color + ", f�r " + this.price + " Euro";
	}

}
