package de.binaris.cleancode.solid.di.good;

import de.binaris.cleancode.solid.di.good.Coffee.Strength;

public class CoffeeServiceUser {

	public static void main(String[] args) {
		CoffeeService coffeeService = new CoffeeService(new CoffeeMachine());
		Coffee coffee = coffeeService.serveCoffee(Strength.MEDIUM, "Espresso");
		System.out.println(coffee);
	}

}
