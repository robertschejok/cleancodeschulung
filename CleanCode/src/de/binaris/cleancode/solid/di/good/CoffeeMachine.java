package de.binaris.cleancode.solid.di.good;

import de.binaris.cleancode.solid.di.good.Coffee.Strength;

public class CoffeeMachine {

	public Coffee makeCoffee(Strength strength, String type) {
		Coffee coffee = new Coffee();
		coffee.setStrength(strength);
		coffee.setType(type);
		return coffee;
	}

}
