package de.binaris.cleancode.solid.di.good;

import de.binaris.cleancode.solid.di.good.Coffee.Strength;

public class CoffeeService {

	private CoffeeMachine coffeeMachine;

	public CoffeeService(CoffeeMachine coffeeMachine) {
		this.coffeeMachine = coffeeMachine;
	}

	public Coffee serveCoffee(Strength strength, String type) {
		return coffeeMachine.makeCoffee(strength, type);
	}
}
