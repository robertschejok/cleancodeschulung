package de.binaris.cleancode.solid.di.good;

public class Coffee {

	public enum Strength {
		WEAK("schwach"), MEDIUM("medium"), STRONG("stark"), VERY_STRONG("sehr stark");
		private final String name;

		private Strength(String name) {
			this.name = name;
		}
	}

	private Strength strength;
	private String type;

	public Strength getStrength() {
		return strength;
	}

	public void setStrength(Strength strength) {
		this.strength = strength;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Kaffe der Sorte " + type + " mit St�rke " + strength;
	}
}
