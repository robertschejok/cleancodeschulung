package de.binaris.cleancode.solid.di.bad;

import de.binaris.cleancode.solid.di.bad.Coffee.Strength;

public class CoffeeService {
	private CoffeeMachine coffeeMachine;
	public Coffee serveCoffee(Strength strength, String type) {
		if(coffeeMachine == null) {
			coffeeMachine = new CoffeeMachine();
		}
		return coffeeMachine.makeCoffee(strength, type);
	}
}
