package de.binaris.cleancode.solid.di.bad;

import de.binaris.cleancode.solid.di.bad.Coffee.Strength;

public class CoffeeServiceUser {
	
	public static void main(String[] args) {
		CoffeeService coffeeService = new CoffeeService();
		Coffee coffee = coffeeService.serveCoffee(Strength.MEDIUM, "Espresso");
		System.out.println(coffee);
	}
}
