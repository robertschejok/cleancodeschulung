package de.binaris.cleancode.solid.lsp.bad;

public class Bicycle extends TransportationDevice {
	public Bicycle(Engine engine, String name) {
		super(null, name);
	}
/*
	@Override
	public void startEngine() {
		throw new RuntimeException("Bicycles don't have engines...");
	}
*/	
	public void startMoving(int speed) {
		setSpeed(speed);
	}
}
