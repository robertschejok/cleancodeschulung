package de.binaris.cleancode.solid.lsp.bad;

public class Motorcycle extends TransportationDevice {
	public Motorcycle(Engine engine, String name) {
		super(engine, name);
	}

	@Override
	public void startEngine() {
		super.startEngine();
		setSpeed(15);
	}
}
