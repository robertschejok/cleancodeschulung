package de.binaris.cleancode.solid.lsp.bad;

public abstract class TransportationDevice {
	
	private String name;
	private int speed;
	private Engine engine;
	
	public TransportationDevice(Engine engine, String name) {
		this.name = name;
		this.setEngine(engine);
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}
	
	public Engine getEngine() {
		return engine;
	}
	
	public void setEngine(Engine engine) {
		this.engine = engine;
	}

	public void startEngine() {
		if(engine == null) {
			System.out.println("No Engine in this TransportationDevice: " + this.getName());
			return;
		}
		if(!this.getEngine().isRunning()) {
			this.getEngine().setRunning(true);
		}
	}
	
	public void stopEngine() {
		if(this.getEngine() != null && this.getEngine().isRunning()) {
			this.getEngine().setRunning(false);
		}
	}

	
}
