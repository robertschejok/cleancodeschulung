package de.binaris.cleancode.solid.lsp.bad;

public class Car extends TransportationDevice{
	
	public Car(Engine engine, String name) {
		super(engine, name);
	}

	@Override
	public void startEngine() {
		super.startEngine();
		this.setSpeed(10);
	}
}
