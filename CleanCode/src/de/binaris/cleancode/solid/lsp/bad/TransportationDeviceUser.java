package de.binaris.cleancode.solid.lsp.bad;

public class TransportationDeviceUser {

	public static void main(String[] args) {
		Car car = new Car(new Engine(), "Trabant");
		car.startEngine();
		System.out.println(car.getName() + " is running at " + car.getSpeed() + "km/h");
		
		Motorcycle motorcycle = new Motorcycle(new Engine(), "Hayabusa");
		motorcycle.startEngine();
		System.out.println(motorcycle.getName() + " is running at " + motorcycle.getSpeed() + "km/h");
		
		Bicycle bicycle = new Bicycle(new Engine(), "Kettler Alu-Rad");
		bicycle.startEngine(); //Nicht gut
		System.out.println(bicycle.getName() + " is running at " + bicycle.getSpeed() + " km/h");
		bicycle.startMoving(5);
		System.out.println(bicycle.getName() + " is running at " + bicycle.getSpeed() + " km/h");
		
		TransportationDevice transportationDevice = new Bicycle(new Engine(), "TransportationDevice");
		transportationDevice.startEngine();
		System.out.println(transportationDevice.getName() + " is running at " + transportationDevice.getSpeed() + " km/h");
	}

}
