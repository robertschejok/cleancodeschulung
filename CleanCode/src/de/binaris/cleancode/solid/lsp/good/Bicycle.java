package de.binaris.cleancode.solid.lsp.good;

public class Bicycle extends TransportationDevice {
	public Bicycle(String name) {
		setName(name);
	}

	public void startMoving(int speed) {
		setSpeed(speed);
	}
}
