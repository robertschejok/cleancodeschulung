package de.binaris.cleancode.solid.lsp.good;

import de.binaris.cleancode.solid.lsp.good.Engine;
import de.binaris.cleancode.solid.lsp.good.TransportationDevice;

public abstract class TransportationDeviceWithEngine extends TransportationDevice {
	
	private Engine engine;
	
	public TransportationDeviceWithEngine(Engine engine, String name) {
		setEngine(engine);
		setName(name);
	}
	
	public Engine getEngine() {
		return engine;
	}
	
	public void setEngine(Engine engine) {
		this.engine = engine;
	}

	public void startEngine() {
		if(engine == null) {
			System.out.println("No Engine in this TransportationDevice: " + this.getName());
			return;
		}
		if(!this.getEngine().isRunning()) {
			this.getEngine().setRunning(true);
		}
	}
	
	public void stopEngine() {
		if(this.getEngine() != null && this.getEngine().isRunning()) {
			this.getEngine().setRunning(false);
		}
	}

	

}
