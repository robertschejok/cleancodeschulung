package de.binaris.cleancode.solid.lsp.good;

public abstract class TransportationDevice {
	
	private String name;
	private int speed;
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}
	
}
