package de.binaris.cleancode.solid.lsp.good;

public class Engine {
	
	private boolean running;

	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}
}
