package de.binaris.cleancode.solid.lsp.good;

public class Car extends TransportationDeviceWithEngine{
	
	public Car(Engine engine, String name) {
		super(engine, name);
	}

	@Override
	public void startEngine() {
		super.startEngine();
		this.setSpeed(10);
	}
}
