package de.binaris.cleancode.solid.lsp.good;

public class Motorcycle extends TransportationDeviceWithEngine {
	public Motorcycle(Engine engine, String name) {
		super(engine, name);
	}

	@Override
	public void startEngine() {
		super.startEngine();
		setSpeed(15);
	}
}
